package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.tsc.ichaplygina.taskmanager.api.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Nullable
    @Override
    @WebMethod
    @PostMapping("/add")
    public Task add(@NotNull @WebParam(name = "task") @RequestBody final Task task) {
        return taskService.write(task);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/removeById/{id}")
    public void removeById(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(@NotNull @WebParam(name = "task") @RequestBody final Task task) {
        if (findById(task.getId()) != null) taskService.write(task);
    }


}
