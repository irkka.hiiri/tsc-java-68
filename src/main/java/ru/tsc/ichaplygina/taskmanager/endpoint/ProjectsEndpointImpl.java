package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.ichaplygina.taskmanager.api.ProjectsEndpoint")
public class ProjectsEndpointImpl implements ProjectsEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @Nullable
    @WebMethod
    @PostMapping("/add")
    public List<Project> add(@WebParam(name = "projects") @RequestBody @NotNull List<Project> projects) {
        return projectService.write(projects);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/remove")
    public void remove(@WebParam(name = "projects") @RequestBody @NotNull List<Project> projects) {
        projectService.remove(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/removeAll")
    public void removeAll() {
        projectService.removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    @PutMapping("/save")
    public List<Project> save(@WebParam(name = "projects") @RequestBody @NotNull List<Project> projects) {
        return projectService.write(projects);
    }
}

