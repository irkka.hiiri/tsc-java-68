package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;


    public List<Project> findAll() {
        return repository.findAll();
    }

    public Project findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void remove(List<Project> projects) {
        repository.deleteAll(projects);
    }

    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Transactional
    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    @Nullable
    @Transactional
    public Project write(@NotNull final Project project) {
        return repository.save(project);
    }

    @Transactional
    @NotNull
    public List<Project> write(@NotNull final List<Project> projects) {
        return repository.saveAll(projects);
    }
}
